<?php
namespace App\Security ;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use App\Entity\User;
class NoToutouChecker implements UserCheckerInterface {
    public function checkPreAuth(UserInterface $user)
    {
        $this->checkAuth($user);
    }
    
    public function checkPostAuth(UserInterface $user) 
    {
        $this->checkAuth($user);
    }
    private function checkAuth(UserInterface $user)
    {
        if(!$user instanceof User)
        {
            return ;
        }
        if(str_contains($user->getUserIdentifier(),'toutou')){
        //     if(str_contains($user->getEmail(),'toutou')){
            throw new CustomUserMessageAccountStatusException('pas de toutous ici !!!');
        }
    }
}